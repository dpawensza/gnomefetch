<img align="right" src="gnome.png" width=30%>

# gnomefetch ![build: passing (probably)](https://img.shields.io/badge/build-passing_(probably)-00d000)
A joke "fetch" program I made because I was *very* bored.

## Why?
Why not?

## Dependencies
- compiler

## Compiling
```
git clone https://github.com/Maritsu/gnomefetch.git # Clone the repository
cd gnomefetch # Change directory
gcc -O3 gnomefetch.c -o gnomefetch # Compile
```

### Installation (add to PATH)
```
sudo install -m755 gnomefetch /usr/bin
```

### Uninstall (remove from PATH)
```
sudo rm /usr/bin/gnomefetch
```

## Usage
```
gnomefetch - a joke fetch program that I made because I was *very* bored.

USAGE
gnomefetch [args]
 -g	Use GNOME logo instead of actual gnome. Default if GNOME is detected as currently used DE.
 -n	Use an actual gnome. Used by default
 -h	Show help text
 -v	Show version
```
